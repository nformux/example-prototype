### How I Built This Prototype

1. I opened __Command Prompt__ and initialized a new Harp project.

  `harp init example-prototype`

1. I navigated to the new project folder.

  `cd example-prototype`

1. I created a directory called __public__ and moved all the project files into it.

1. I created __harp.json__ in the project root folder to store globally available data.

1. I created __public/\_data.json__ to store data for individual pages.

1. I moved __main.less__ into __public/css/__

1. I downloaded the latest version of Bootstrap and copied all the Less files into __public/css/\_boostrap__

1. I deleted all the default CSS in __main.less__ and imported the Bootstrap Less files.

  `@import '_bootstrap/bootstrap.less';`

1. I ran the Harp local server and viewed the site at __http://localhost:9000/__.

  `harp server`

1. I created Markdown files for the all of the site pages.

1. I created partials for the main nav, a list of thumbnails, and a paragraph of content and saved them in the __public/\_partials__ folder.

1. I put data for the main nav in the __harp.json__ file.

1. I put data for the list of thumbnails in the __public/\_partials__ folder.

1. I built the HTML/CSS for my prototype.

  `harp compile`

1. I installed Surge for quick deployment.

  `npm install --global surge`

1. I deployed the site.

  `surge ./www --domain example-prototype.surge.sh`
