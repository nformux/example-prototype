### Installing Harp

1. Download and install the latest version of [Node](https://nodejs.org/en/).

2. Open __Command Prompt__ and install Harp using the __npm__ command.

  `npm install --global harp`

---

### Creating a new Harp project

1. Open __Command Prompt__ and run Harp init.

  `harp init your-project-name`

---

### Running a local Harp server

1. Open __Command Prompt__ and navigate to the project folder.

  `cd your-project-name`

2. Begin Harp server.

  `harp server`

3. Navigate to __http://localhost:9000/__ in your browser.

---

### Compiling a Harp project

1. Open __Command Prompt__ and navigate to the project folder.

  `cd your-project-name`

2. Run Harp compile.

  `harp compile`

3. View the built HTML and CSS in the __www__ folder.

---

<a class="btn btn-default" href="http://harpjs.com/docs">Read the Harp Documentation</a>
